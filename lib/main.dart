import 'package:flutter/material.dart';

import 'screens/screens.dart';
import 'widgets/widgets.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Claze',
      theme: ThemeData(
        useMaterial3: true,
        textTheme: TextTheme(
          bodySmall: TextStyle(
            fontWeight: FontWeight.w800,
            foreground: Paint()
              ..style = PaintingStyle.stroke
              ..strokeWidth = 3
              ..color = Colors.black,
            decoration: TextDecoration.none,
          ),
        ),
      ),
      debugShowCheckedModeBanner: false,
      home: const BodyWithBackgroundImage(child: SplashScreen()),
    );
  }
}

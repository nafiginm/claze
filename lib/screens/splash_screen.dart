import 'dart:async';

import 'package:flutter/material.dart';

import 'home_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});
  static const String routeName = '/splash';

  static Route route() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (context) => const SplashScreen(),
    );
  }

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();

    Timer(
      const Duration(seconds: 2),
      () {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => const HomeScreen(),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Stack(
          children: <Widget>[
            Text(
              'Loading',
              style: Theme.of(context).textTheme.bodySmall!.copyWith(
                    fontSize: 50,
                  ),
            ),
            Text(
              'Loading',
              style: Theme.of(context).textTheme.bodySmall!.copyWith(
                    fontSize: 50,
                    foreground: Paint()..color = const Color(0xffFFF101),
                  ),
            ),
          ],
        ),
        const SizedBox(
          height: 118,
        )
      ],
    );
  }
}

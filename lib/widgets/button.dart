import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  const Button({super.key, required this.text, required this.size});

  final String text;
  final double size;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: MediaQuery.of(context).size.height / 15,
      width: MediaQuery.of(context).size.width / 2.2,
      decoration: BoxDecoration(
        color: const Color(0xff110E0E).withOpacity(0.52),
        border: Border.all(
          color: Colors.white,
          width: 3,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Stack(
        children: <Widget>[
          FittedBox(
            fit: BoxFit.contain,
            child: Text(
              text,
              style: Theme.of(context).textTheme.bodySmall!.copyWith(
                    fontSize: size,
                  ),
              softWrap: true,
              textAlign: TextAlign.center,
            ),
          ),
          FittedBox(
            fit: BoxFit.contain,
            child: Text(
              text,
              style: Theme.of(context).textTheme.bodySmall!.copyWith(
                    fontSize: size,
                    foreground: Paint()..color = const Color(0xffFFF101),
                  ),
              softWrap: true,
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }
}

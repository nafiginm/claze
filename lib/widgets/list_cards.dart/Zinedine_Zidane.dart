import 'package:flutter/material.dart';

import '../widgets.dart';

class ZinedineZidane extends StatelessWidget {
  const ZinedineZidane(
      {super.key, required this.nextPage, required this.backPage});

  final backPage;
  final nextPage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            GestureDetector(
              onTap: backPage,
              child: Image.asset(
                'lib/images/back.png',
                height: MediaQuery.of(context).size.width * 0.17,
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            const Button(
              text: "Best Football \n Players of the '90s",
              size: 20,
            ),
          ],
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                Stack(
                  children: <Widget>[
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Zinedine Zidane',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                            ),
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Zinedine Zidane',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                              foreground: Paint()..color = Colors.white,
                            ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Image.asset('lib/images/zidane.png'),
                    const SizedBox(
                      height: 10,
                    ),
                    SingleChildScrollView(
                      child: Stack(
                        children: <Widget>[
                          Text(
                            "Zinedine Zidane, a name synonymous with footballing genius and elegance on the pitch, heralds from Marseille, France. Born on June 23, 1972, to Algerian immigrants, his journey from the gritty streets of La Castellane to the pinnacle of global football is nothing short of inspirational. A naturally gifted player, Zidane's career began at AS Cannes where his remarkable talent was first noticed. His skills propelled him to join Girondins de Bordeaux, and eventually to Juventus, an Italian football powerhouse. At Juventus, Zidane's reputation and influence grew exponentially as he guided the team to two Serie A titles and a UEFA Champions League final. Internationally, Zidane's impact was equally profound.",
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                    ),
                          ),
                          Text(
                            "Zinedine Zidane, a name synonymous with footballing genius and elegance on the pitch, heralds from Marseille, France. Born on June 23, 1972, to Algerian immigrants, his journey from the gritty streets of La Castellane to the pinnacle of global football is nothing short of inspirational. A naturally gifted player, Zidane's career began at AS Cannes where his remarkable talent was first noticed. His skills propelled him to join Girondins de Bordeaux, and eventually to Juventus, an Italian football powerhouse. At Juventus, Zidane's reputation and influence grew exponentially as he guided the team to two Serie A titles and a UEFA Champions League final. Internationally, Zidane's impact was equally profound.",
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                      foreground: Paint()..color = Colors.white,
                                    ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        GestureDetector(
          onTap: nextPage,
          child: const Button(
            text: 'Next',
            size: 35,
          ),
        ),
      ],
    );
  }
}

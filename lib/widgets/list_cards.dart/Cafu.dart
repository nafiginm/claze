import 'package:flutter/material.dart';

import '../widgets.dart';

class Cafu extends StatelessWidget {
  const Cafu({super.key, required this.backPage, required this.nextPage});

  final backPage;
  final nextPage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            GestureDetector(
              onTap: backPage,
              child: Image.asset(
                'lib/images/back.png',
                height: MediaQuery.of(context).size.width * 0.17,
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            const Button(
              text: "Best Football \n Players of the '90s",
              size: 20,
            ),
          ],
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                Stack(
                  children: <Widget>[
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Cafu',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                            ),
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Cafu',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                              foreground: Paint()..color = Colors.white,
                            ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Image.asset('lib/images/cafu.png'),
                    const SizedBox(
                      height: 10,
                    ),
                    SingleChildScrollView(
                      child: Stack(
                        children: <Widget>[
                          Text(
                            "Marcos Evangelista de Morais (born 7 June 1970), known as Cafu [kaˈfu], is a Brazilian former professional footballer who played as a defender. With 142 appearances for the Brazil national team, he is the most internationally capped Brazilian player of all time. He represented his nation in four FIFA World Cups between 1994 and 2006, and is the only player to have appeared in three consecutive World Cup finals, winning the 1994 and 2002 editions of the tournament, the latter as his team's captain where he lifted the World Cup trophy. With Brazil, he also took part in four editions of the Copa América, winning the title twice, in 1997 and 1999; he was also a member of the national side that won the 1997 FIFA Confederations Cup.",
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                    ),
                          ),
                          Text(
                            "Marcos Evangelista de Morais (born 7 June 1970), known as Cafu [kaˈfu], is a Brazilian former professional footballer who played as a defender. With 142 appearances for the Brazil national team, he is the most internationally capped Brazilian player of all time. He represented his nation in four FIFA World Cups between 1994 and 2006, and is the only player to have appeared in three consecutive World Cup finals, winning the 1994 and 2002 editions of the tournament, the latter as his team's captain where he lifted the World Cup trophy. With Brazil, he also took part in four editions of the Copa América, winning the title twice, in 1997 and 1999; he was also a member of the national side that won the 1997 FIFA Confederations Cup.",
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                      foreground: Paint()..color = Colors.white,
                                    ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        GestureDetector(
          onTap: nextPage,
          child: const Button(
            text: 'Next',
            size: 35,
          ),
        ),
      ],
    );
  }
}

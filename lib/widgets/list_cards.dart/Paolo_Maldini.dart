import 'package:flutter/material.dart';

import '../widgets.dart';

class PaoloMaldini extends StatelessWidget {
  const PaoloMaldini(
      {super.key, required this.backPage, required this.nextPage});

  final backPage;
  final nextPage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            GestureDetector(
              onTap: backPage,
              child: Image.asset(
                'lib/images/back.png',
                height: MediaQuery.of(context).size.width * 0.17,
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            const Button(
              text: "Best Football \n Players of the '90s",
              size: 20,
            ),
          ],
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                Stack(
                  children: <Widget>[
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Paolo Maldini',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                            ),
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Paolo Maldini',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                              foreground: Paint()..color = Colors.white,
                            ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Image.asset('lib/images/maldini.png'),
                    const SizedBox(
                      height: 10,
                    ),
                    SingleChildScrollView(
                      child: Stack(
                        children: <Widget>[
                          Text(
                            "Paolo Cesare Maldini is an Italian former professional footballer who played as a left back and central defender for A.C. Milan and the Italy national team, who is widely regarded as one of the greatest ever defenders, and as one of the greatest players of all time. He is currently serving as technical director for Milan, as well as being co-owner of North American Soccer League (NASL) club Miami FC. During his playing career, he spent all 25 seasons of his career in the Serie A with Milan, before retiring at the age of 41 in 2009. He won 25 trophies with Milan: the UEFA Champions League five times, seven Serie A titles, one Coppa Italia, five Supercoppa Italiana titles, five European Super Cups, two Intercontinental Cups and one FIFA Club World Cup.",
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                    ),
                          ),
                          Text(
                            "Paolo Cesare Maldini is an Italian former professional footballer who played as a left back and central defender for A.C. Milan and the Italy national team, who is widely regarded as one of the greatest ever defenders, and as one of the greatest players of all time. He is currently serving as technical director for Milan, as well as being co-owner of North American Soccer League (NASL) club Miami FC. During his playing career, he spent all 25 seasons of his career in the Serie A with Milan, before retiring at the age of 41 in 2009. He won 25 trophies with Milan: the UEFA Champions League five times, seven Serie A titles, one Coppa Italia, five Supercoppa Italiana titles, five European Super Cups, two Intercontinental Cups and one FIFA Club World Cup.",
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                      foreground: Paint()..color = Colors.white,
                                    ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        GestureDetector(
          onTap: nextPage,
          child: const Button(
            text: 'Next',
            size: 35,
          ),
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';

import '../widgets.dart';

class GheorgheHagi extends StatelessWidget {
  const GheorgheHagi(
      {super.key, required this.backPage, required this.nextPage});

  final backPage;
  final nextPage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            GestureDetector(
              onTap: backPage,
              child: Image.asset(
                'lib/images/back.png',
                height: MediaQuery.of(context).size.width * 0.17,
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            const Button(
              text: "Best Football \n Players of the '90s",
              size: 20,
            ),
          ],
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                Stack(
                  children: <Widget>[
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Gheorghe Hagi',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                            ),
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Gheorghe Hagi',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                              foreground: Paint()..color = Colors.white,
                            ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Image.asset('lib/images/hagi.png'),
                    const SizedBox(
                      height: 10,
                    ),
                    SingleChildScrollView(
                      child: Stack(
                        children: <Widget>[
                          Text(
                            "Gheorghe Hagi (Romanian pronunciation: [ˈɡe̯orɡe ˈhad͡ʒi] (listen); born 5 February 1965) is a Romanian former professional footballer, considered one of the best players in the world during the 1980s and '90s and the greatest Romanian footballer of all time. Galatasaray fans called him \"Comandante\" (\"The Commander\") and Romanians call him \"Regele\" (\"The King\"). He is currently the owner and manager of Viitorul Constanța. Hagi is considered a hero in his homeland. He was named Romanian Footballer of the Year seven times, and is regarded as one of the best football players of his generation. Nicknamed \"The Maradona of the Carpathians\"",
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                    ),
                          ),
                          Text(
                            "Gheorghe Hagi (Romanian pronunciation: [ˈɡe̯orɡe ˈhad͡ʒi] (listen); born 5 February 1965) is a Romanian former professional footballer, considered one of the best players in the world during the 1980s and '90s and the greatest Romanian footballer of all time. Galatasaray fans called him \"Comandante\" (\"The Commander\") and Romanians call him \"Regele\" (\"The King\"). He is currently the owner and manager of Viitorul Constanța. Hagi is considered a hero in his homeland. He was named Romanian Footballer of the Year seven times, and is regarded as one of the best football players of his generation. Nicknamed \"The Maradona of the Carpathians\"",
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                      foreground: Paint()..color = Colors.white,
                                    ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        GestureDetector(
          onTap: nextPage,
          child: const Button(
            text: 'Next',
            size: 35,
          ),
        ),
      ],
    );
  }
}

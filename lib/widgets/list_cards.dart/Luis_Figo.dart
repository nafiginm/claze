import 'package:flutter/material.dart';

import '../widgets.dart';

class LuisFigo extends StatelessWidget {
  const LuisFigo({super.key, required this.backPage, required this.nextPage});

  final backPage;
  final nextPage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            GestureDetector(
              onTap: backPage,
              child: Image.asset(
                'lib/images/back.png',
                height: MediaQuery.of(context).size.width * 0.17,
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            const Button(
              text: "Best Football \n Players of the '90s",
              size: 20,
            ),
          ],
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                Stack(
                  children: <Widget>[
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Luis Figo',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                            ),
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Luis Figo',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                              foreground: Paint()..color = Colors.white,
                            ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Image.asset('lib/images/figo.png'),
                    const SizedBox(
                      height: 10,
                    ),
                    SingleChildScrollView(
                      child: Stack(
                        children: <Widget>[
                          Text(
                            "Luís Filipe Madeira Caeiro Figo OIH (Portuguese pronunciation: [luˈiʃ ˈfiɣu]; born 4 November 1972) is a retired Portuguese professional footballer who played as a midfielder for Sporting CP, Barcelona, Real Madrid and Inter Milan before retiring on 31 May 2009. He won 127 caps for the Portugal national team, a record at the time but later broken by Cristiano Ronaldo. Renowned for his creativity and ability to get past defenders as a winger, Figo is regarded as one of the greatest players of his generation. His 106 assists are the second-most in La Liga history, behind Lionel Messi. He won the 2000 Ballon d'Or, 2001 FIFA World Player of the Year, and in 2004 Pelé named him in the FIFA 100 list of the world's greatest living players.",
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                    ),
                          ),
                          Text(
                            "Luís Filipe Madeira Caeiro Figo OIH (Portuguese pronunciation: [luˈiʃ ˈfiɣu]; born 4 November 1972) is a retired Portuguese professional footballer who played as a midfielder for Sporting CP, Barcelona, Real Madrid and Inter Milan before retiring on 31 May 2009. He won 127 caps for the Portugal national team, a record at the time but later broken by Cristiano Ronaldo. Renowned for his creativity and ability to get past defenders as a winger, Figo is regarded as one of the greatest players of his generation. His 106 assists are the second-most in La Liga history, behind Lionel Messi. He won the 2000 Ballon d'Or, 2001 FIFA World Player of the Year, and in 2004 Pelé named him in the FIFA 100 list of the world's greatest living players.",
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                      foreground: Paint()..color = Colors.white,
                                    ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        GestureDetector(
          onTap: nextPage,
          child: const Button(
            text: 'Next',
            size: 35,
          ),
        ),
      ],
    );
  }
}

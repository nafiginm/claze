import 'package:flutter/material.dart';

import '../widgets.dart';

class ThierryHenry extends StatelessWidget {
  const ThierryHenry({
    super.key,
    required this.backPage,
    required this.nextPage,
  });

  final backPage;
  final nextPage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            GestureDetector(
              onTap: backPage,
              child: Image.asset(
                'lib/images/back.png',
                height: MediaQuery.of(context).size.width * 0.17,
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            const Button(
              text: "Best Football \n Players of the '90s",
              size: 20,
            ),
          ],
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                Stack(
                  children: <Widget>[
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Thierry Henry',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                            ),
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Thierry Henry',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                              foreground: Paint()..color = Colors.white,
                            ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Image.asset('lib/images/henry.png'),
                    const SizedBox(
                      height: 10,
                    ),
                    SingleChildScrollView(
                      child: Stack(
                        children: <Widget>[
                          Text(
                            'Thierry Daniel Henry (French pronunciation: ​[tjɛʁi ɑ̃ʁi]; born 17 August 1977) is a French professional football coach and former player who was most recently the manager of Ligue 1 club Monaco. Considered one of the best strikers of all-time, Henry made his professional debut with Monaco in 1994, where good form led to an international call-up in 1998, after which he signed for defending Serie A champions Juventus. Limited playing time at Juve where he was played out of position on the wing allowed him to sign for Premier League club Arsenal for £11 million in 1999. It was at Arsenal that Henry made his name as a world-class player.',
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                    ),
                          ),
                          Text(
                            'Thierry Daniel Henry (French pronunciation: ​[tjɛʁi ɑ̃ʁi]; born 17 August 1977) is a French professional football coach and former player who was most recently the manager of Ligue 1 club Monaco. Considered one of the best strikers of all-time, Henry made his professional debut with Monaco in 1994, where good form led to an international call-up in 1998, after which he signed for defending Serie A champions Juventus. Limited playing time at Juve where he was played out of position on the wing allowed him to sign for Premier League club Arsenal for £11 million in 1999. It was at Arsenal that Henry made his name as a world-class player.',
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                      foreground: Paint()..color = Colors.white,
                                    ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        GestureDetector(
          onTap: nextPage,
          child: const Button(
            text: 'Next',
            size: 35,
          ),
        ),
      ],
    );
  }
}

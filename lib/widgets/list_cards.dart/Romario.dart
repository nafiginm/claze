import 'package:flutter/material.dart';

import '../widgets.dart';

class Romario extends StatelessWidget {
  const Romario({
    super.key,
    required this.backPage,
    required this.nextPage,
  });

  final backPage;
  final nextPage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            GestureDetector(
              onTap: backPage,
              child: Image.asset(
                'lib/images/back.png',
                height: MediaQuery.of(context).size.width * 0.17,
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            const Button(
              text: "Best Football \n Players of the '90s",
              size: 20,
            ),
          ],
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                Stack(
                  children: <Widget>[
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Romario',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                            ),
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Romario',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                              foreground: Paint()..color = Colors.white,
                            ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Image.asset('lib/images/romario.png'),
                    const SizedBox(
                      height: 10,
                    ),
                    SingleChildScrollView(
                      child: Stack(
                        children: <Widget>[
                          Text(
                            "Romário de Souza Faria (born 29 January 1966), known simply as Romário (Brazilian Portuguese: [ʁoˈmaɾiu]), is a Brazilian politician who previously achieved worldwide fame as a professional footballer. A prolific striker renowned for his clinical finishing, he is regarded as one of the greatest players of all time. Romário starred for Brazil in their 1994 FIFA World Cup success, receiving the FIFA Golden Ball as player of the tournament. He was named FIFA World Player of the Year the same year. He came fifth in the FIFA Player of the Century internet poll in 1999, was elected to the FIFA World Cup Dream Team in 2002, and was named in the FIFA 100 list of the world's greatest living players in 2004.",
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                    ),
                          ),
                          Text(
                            "Romário de Souza Faria (born 29 January 1966), known simply as Romário (Brazilian Portuguese: [ʁoˈmaɾiu]), is a Brazilian politician who previously achieved worldwide fame as a professional footballer. A prolific striker renowned for his clinical finishing, he is regarded as one of the greatest players of all time. Romário starred for Brazil in their 1994 FIFA World Cup success, receiving the FIFA Golden Ball as player of the tournament. He was named FIFA World Player of the Year the same year. He came fifth in the FIFA Player of the Century internet poll in 1999, was elected to the FIFA World Cup Dream Team in 2002, and was named in the FIFA 100 list of the world's greatest living players in 2004.",
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                      foreground: Paint()..color = Colors.white,
                                    ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        GestureDetector(
          onTap: nextPage,
          child: const Button(
            text: 'Next',
            size: 35,
          ),
        ),
      ],
    );
  }
}

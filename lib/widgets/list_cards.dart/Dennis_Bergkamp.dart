import 'package:flutter/material.dart';

import '../widgets.dart';

class DennisBergkamp extends StatelessWidget {
  const DennisBergkamp(
      {super.key, required this.backPage, required this.nextPage});

  final backPage;
  final nextPage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            GestureDetector(
              onTap: backPage,
              child: Image.asset(
                'lib/images/back.png',
                height: MediaQuery.of(context).size.width * 0.17,
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            const Button(
              text: "Best Football \n Players of the '90s",
              size: 20,
            ),
          ],
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                Stack(
                  children: <Widget>[
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Dennis Bergkamp',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                            ),
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Dennis Bergkamp',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                              foreground: Paint()..color = Colors.white,
                            ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Image.asset('lib/images/bergkamp.png'),
                    const SizedBox(
                      height: 10,
                    ),
                    SingleChildScrollView(
                      child: Stack(
                        children: <Widget>[
                          Text(
                            'Dennis Nicolaas Maria Bergkamp (Dutch pronunciation: [ˈdɛnəs ˈbɛrxkɑmp] (listen); born 10 May 1969) is a Dutch former professional footballer, who until 21 December 2017 was the assistant manager at Ajax. Originally a wide midfielder, Bergkamp was moved to main striker and then to second striker, where he remained throughout his playing career. Bergkamp has been described by Jan Mulder as having "the finest technique" of any Dutch international and a "dream for a striker" by teammate Thierry Henry.The son of an electrician, Bergkamp was born in Amsterdam and played as an amateur in the lower leagues. He was spotted by Ajax at age 11 and made his professional debut in 1986.',
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                    ),
                          ),
                          Text(
                            'Dennis Nicolaas Maria Bergkamp (Dutch pronunciation: [ˈdɛnəs ˈbɛrxkɑmp] (listen); born 10 May 1969) is a Dutch former professional footballer, who until 21 December 2017 was the assistant manager at Ajax. Originally a wide midfielder, Bergkamp was moved to main striker and then to second striker, where he remained throughout his playing career. Bergkamp has been described by Jan Mulder as having "the finest technique" of any Dutch international and a "dream for a striker" by teammate Thierry Henry.The son of an electrician, Bergkamp was born in Amsterdam and played as an amateur in the lower leagues. He was spotted by Ajax at age 11 and made his professional debut in 1986.',
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                      foreground: Paint()..color = Colors.white,
                                    ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        GestureDetector(
          onTap: nextPage,
          child: const Button(
            text: 'Next',
            size: 35,
          ),
        ),
      ],
    );
  }
}

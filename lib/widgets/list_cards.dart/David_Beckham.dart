import 'package:flutter/material.dart';

import '../widgets.dart';

class DavidBeckham extends StatelessWidget {
  const DavidBeckham(
      {super.key, required this.backPage, required this.nextPage});

  final backPage;
  final nextPage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            GestureDetector(
              onTap: backPage,
              child: Image.asset(
                'lib/images/back.png',
                height: MediaQuery.of(context).size.width * 0.17,
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            const Button(
              text: "Best Football \n Players of the '90s",
              size: 20,
            ),
          ],
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                Stack(
                  children: <Widget>[
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'David Beckham',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                            ),
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'David Beckham',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                              foreground: Paint()..color = Colors.white,
                            ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Image.asset('lib/images/beckham.png'),
                    const SizedBox(
                      height: 10,
                    ),
                    SingleChildScrollView(
                      child: Stack(
                        children: <Widget>[
                          Text(
                            "David Beckham, born on May 2, 1975 in London, England, is a globally recognized figure in the realm of professional football. The son of a kitchen fitter and a hairdresser, Beckham's passion for football was ignited at an early age, leading him to play for several youth teams before he was noticed by Manchester United scouts. His professional journey began when he signed a contract with Manchester United at the age of 16, marking the start of an illustrious career that would span over two decades. Beckham's tenure at Manchester United was characterized by a string of successes. With his exceptional talent and precision, he helped the team secure numerous victories including six Premier League titles and the prestigious UEFA Champions League.",
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                    ),
                          ),
                          Text(
                            "David Beckham, born on May 2, 1975 in London, England, is a globally recognized figure in the realm of professional football. The son of a kitchen fitter and a hairdresser, Beckham's passion for football was ignited at an early age, leading him to play for several youth teams before he was noticed by Manchester United scouts. His professional journey began when he signed a contract with Manchester United at the age of 16, marking the start of an illustrious career that would span over two decades. Beckham's tenure at Manchester United was characterized by a string of successes. With his exceptional talent and precision, he helped the team secure numerous victories including six Premier League titles and the prestigious UEFA Champions League.",
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                      foreground: Paint()..color = Colors.white,
                                    ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        GestureDetector(
          onTap: nextPage,
          child: const Button(
            text: 'Next',
            size: 35,
          ),
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';

import '../widgets.dart';

class Rivaldo extends StatelessWidget {
  const Rivaldo({super.key, required this.backPage, required this.nextPage});

  final backPage;
  final nextPage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            GestureDetector(
              onTap: backPage,
              child: Image.asset(
                'lib/images/back.png',
                height: MediaQuery.of(context).size.width * 0.17,
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            const Button(
              text: "Best Football \n Players of the '90s",
              size: 20,
            ),
          ],
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                Stack(
                  children: <Widget>[
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Rivaldo',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                            ),
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Rivaldo',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                              foreground: Paint()..color = Colors.white,
                            ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Image.asset('lib/images/rivaldo.png'),
                    const SizedBox(
                      height: 10,
                    ),
                    SingleChildScrollView(
                      child: Stack(
                        children: <Widget>[
                          Text(
                            "Rivaldo Vítor Barbosa Ferreira (born 19 April 1972), known as Rivaldo (Brazilian Portuguese: [ʁiˈvawdu]), is a Brazilian former professional footballer. He played mainly as an attacking midfielder but also as a second striker. Although predominantly left footed, he was capable of playing on either flank, and was on occasion deployed as a wide midfielder or as a winger.He spent five years with Spanish club Barcelona, where he formed a successful partnership with Patrick Kluivert, and won the 1998 and 1999 Spanish La Liga championship and the 1998 Copa del Rey. With 130 goals for Barcelona he is the club's ninth highest goalscorer.",
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                    ),
                          ),
                          Text(
                            "Rivaldo Vítor Barbosa Ferreira (born 19 April 1972), known as Rivaldo (Brazilian Portuguese: [ʁiˈvawdu]), is a Brazilian former professional footballer. He played mainly as an attacking midfielder but also as a second striker. Although predominantly left footed, he was capable of playing on either flank, and was on occasion deployed as a wide midfielder or as a winger.He spent five years with Spanish club Barcelona, where he formed a successful partnership with Patrick Kluivert, and won the 1998 and 1999 Spanish La Liga championship and the 1998 Copa del Rey. With 130 goals for Barcelona he is the club's ninth highest goalscorer.",
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                      foreground: Paint()..color = Colors.white,
                                    ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        GestureDetector(
          onTap: nextPage,
          child: const Button(
            text: 'Next',
            size: 35,
          ),
        ),
      ],
    );
  }
}

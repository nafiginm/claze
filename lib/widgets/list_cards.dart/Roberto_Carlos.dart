import 'package:flutter/material.dart';

import '../widgets.dart';

class RobertoCarlos extends StatelessWidget {
  const RobertoCarlos(
      {super.key, required this.backPage, required this.nextPage});

  final backPage;
  final nextPage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            GestureDetector(
              onTap: backPage,
              child: Image.asset(
                'lib/images/back.png',
                height: MediaQuery.of(context).size.width * 0.17,
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            const Button(
              text: "Best Football \n Players of the '90s",
              size: 20,
            ),
          ],
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                Stack(
                  children: <Widget>[
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Roberto Carlos',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                            ),
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Roberto Carlos',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                              foreground: Paint()..color = Colors.white,
                            ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Image.asset('lib/images/carlos.png'),
                    const SizedBox(
                      height: 10,
                    ),
                    SingleChildScrollView(
                      child: Stack(
                        children: <Widget>[
                          Text(
                            'Roberto Carlos da Silva Rocha (born 10 April 1973), more commonly known simply as Roberto Carlos, is a Brazilian retired professional footballer who now works as a football ambassador. He started his career in Brazil as a forward but spent most of his career as a left back and has been described as the "most offensive-minded left-back in the history of the game". Carlos is also widely considered one of the best left backs in history.At club level, he joined Real Madrid in 1996 where he spent 11 highly successful seasons, playing 584 matches in all competitions and scoring 71 goals. At Real, he won four La Liga titles and the UEFA Champions League three times.',
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                    ),
                          ),
                          Text(
                            'Roberto Carlos da Silva Rocha (born 10 April 1973), more commonly known simply as Roberto Carlos, is a Brazilian retired professional footballer who now works as a football ambassador. He started his career in Brazil as a forward but spent most of his career as a left back and has been described as the "most offensive-minded left-back in the history of the game". Carlos is also widely considered one of the best left backs in history.At club level, he joined Real Madrid in 1996 where he spent 11 highly successful seasons, playing 584 matches in all competitions and scoring 71 goals. At Real, he won four La Liga titles and the UEFA Champions League three times.',
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                      foreground: Paint()..color = Colors.white,
                                    ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        GestureDetector(
          onTap: nextPage,
          child: const Button(
            text: 'Next',
            size: 35,
          ),
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';

import '../widgets.dart';

class Ronaldo extends StatelessWidget {
  const Ronaldo({super.key, required this.nextPage, required this.backPage});

  final backPage;
  final nextPage;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            GestureDetector(
              onTap: backPage,
              child: Image.asset(
                'lib/images/back.png',
                height: MediaQuery.of(context).size.width * 0.17,
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            const Button(
              text: "Best Football \n Players of the '90s",
              size: 20,
            ),
          ],
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                Stack(
                  children: <Widget>[
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Ronaldo',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                            ),
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Ronaldo',
                        style: Theme.of(context).textTheme.bodySmall!.copyWith(
                              fontSize: 35,
                              foreground: Paint()..color = Colors.white,
                            ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Image.asset('lib/images/ronaldo.png'),
                    const SizedBox(
                      height: 10,
                    ),
                    SingleChildScrollView(
                      child: Stack(
                        children: <Widget>[
                          Text(
                            "Ronaldo Luís Nazário de Lima (born 18 September 1976), commonly known as Ronaldo or Ronaldo Nazário, is a Brazilian business owner, president of La Liga club Real Valladolid, owner of Brasileiro Série B club Cruzeiro and a retired professional footballer who played as a striker. Popularly dubbed O Fenômeno ('The Phenomenon') and also nicknamed R9, he is widely considered as one of the greatest players of all time. As a multi-functional striker who brought a new dimension to the position, Ronaldo has been the influence for a generation of strikers that have followed. His individual accolades include being named FIFA World Player of the Year three times and winning two Ballon d'Or awards.",
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                    ),
                          ),
                          Text(
                            "Ronaldo Luís Nazário de Lima (born 18 September 1976), commonly known as Ronaldo or Ronaldo Nazário, is a Brazilian business owner, president of La Liga club Real Valladolid, owner of Brasileiro Série B club Cruzeiro and a retired professional footballer who played as a striker. Popularly dubbed O Fenômeno ('The Phenomenon') and also nicknamed R9, he is widely considered as one of the greatest players of all time. As a multi-functional striker who brought a new dimension to the position, Ronaldo has been the influence for a generation of strikers that have followed. His individual accolades include being named FIFA World Player of the Year three times and winning two Ballon d'Or awards.",
                            style:
                                Theme.of(context).textTheme.bodySmall!.copyWith(
                                      fontSize: 17,
                                      foreground: Paint()..color = Colors.white,
                                    ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        GestureDetector(
          onTap: nextPage,
          child: const Button(
            text: 'Next',
            size: 35,
          ),
        ),
      ],
    );
  }
}
